package sample;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		DateSorter dateSorter = new DateSorter();

		/*
		 * From example
		 * Result must be:
		 * (2005-01-01, 2005-01-02, 2005-07-01, 2005-05-03)
		 * 
		 * Actual result:
		 * [2005-01-01, 2005-01-02, 2005-07-01, 2005-05-03]
		 * */
		List<LocalDate> localDates = new ArrayList<>() {{
			add(LocalDate.of(2005, 07, 01));
			add(LocalDate.of(2005, 01, 02));
			add(LocalDate.of(2005, 01, 01));
			add(LocalDate.of(2005, 05, 03));
		}};
		
		/*
		 * My test
		 * Result must be:
		 * 2001, 1, 1
		 * 2005, 1, 1
		 * 2005, 1, 2
		 * 2021, 1, 1
		 * 2021, 1, 10
		 * 2023, 2, 1
		 * 2023, 3, 2
		 * 2023, 4, 1
		 * 2023, 9, 2
		 * 2023, 10, 3
		 * 2023, 11, 3
		 * 2023, 12, 1
		 * 
		 * 2023, 8, 1
		 * 2023, 6, 3
		 * 2021, 5, 12
		 * 2021, 5, 1
		 * 2005, 7, 1
		 * 2005, 5, 3
		 * 2001, 5, 12
		 * 2001, 5, 1
		 * 
		 * Actual result:
		 * [2001-01-01
		 * , 2005-01-01
		 * , 2005-01-02
		 * , 2021-01-01
		 * , 2021-01-10
		 * , 2023-02-01
		 * , 2023-03-02
		 * , 2023-04-01
		 * , 2023-09-02
		 * , 2023-10-03
		 * , 2023-11-03
		 * , 2023-12-01
		 * 
		 * , 2023-08-01
		 * , 2023-06-03
		 * , 2021-05-12
		 * , 2021-05-01
		 * , 2005-07-01
		 * , 2005-05-03
		 * , 2001-05-12
		 * , 2001-05-01]
		 * */
		List<LocalDate> localDates2 = new ArrayList<>() {{
			add(LocalDate.of(2005, 7, 1));
			add(LocalDate.of(2005, 1, 2));
			add(LocalDate.of(2005, 1, 1));
			add(LocalDate.of(2005, 5, 3));
			
			add(LocalDate.of(2023, 8, 1));
			add(LocalDate.of(2023, 9, 2));
			add(LocalDate.of(2023, 12, 1));
			add(LocalDate.of(2023, 11, 3));
			
			add(LocalDate.of(2023, 2, 1));
			add(LocalDate.of(2023, 3, 2));
			add(LocalDate.of(2023, 4, 1));
			add(LocalDate.of(2023, 6, 3));
			
			add(LocalDate.of(2023, 10, 3));
			add(LocalDate.of(2001, 1, 1));
			add(LocalDate.of(2001, 5, 1));
			add(LocalDate.of(2001, 5, 12));
			
			add(LocalDate.of(2021, 1, 10));
			add(LocalDate.of(2021, 1, 1));
			add(LocalDate.of(2021, 5, 1));
			add(LocalDate.of(2021, 5, 12));
			
		}};
		
		System.out.println(dateSorter.sortDates(localDates));
		System.out.println(dateSorter.sortDates(localDates2));
	}

}
